package mongodb

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoDbConnection struct {
	Url      string
	Database string
}

var Connection = make(map[string]*MongoDbConnection)

func AddConnection(MongodbUrl string, MongodbDatabase string) {
	Connection[MongodbDatabase] = &MongoDbConnection{
		Url:      MongodbUrl,
		Database: MongodbDatabase,
	}
}

func (a *MongoDbConnection) ConnectDB() (*mongo.Client, *mongo.Database) {
	ctx := context.Background()
	clientOptions := options.Client()
	clientOptions.ApplyURI(a.Url).SetReadPreference(readpref.SecondaryPreferred()).
		SetAppName("catalog").
		SetMaxConnIdleTime(time.Microsecond * 10000)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		panic("cannot connect to mongodb (NewClient) =>" + err.Error())
	}

	err = client.Connect(ctx)
	if err != nil {
		panic("cannot connect to mongodb =>" + err.Error())
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		panic(err)
	}

	//defer client.Disconnect(ctx)
	return client, client.Database(a.Database)
}
