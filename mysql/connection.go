package mysql

import (
	"database/sql"
	"errors"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type GormModel gorm.Model
type GormDeletedAt gorm.DeletedAt

type MysqlConInt interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
	Close() error
}

type GormInt interface {
	Create(value interface{}) (tx *gorm.DB)
	First(dest interface{}, conds ...interface{}) (tx *gorm.DB)
	Model(value interface{}) (tx *gorm.DB)
	Update(column string, value interface{}) (tx *gorm.DB)
	Delete(value interface{}, conds ...interface{}) (tx *gorm.DB)
	AutoMigrate(dst ...interface{}) error
	Find(dest interface{}, conds ...interface{}) (tx *gorm.DB)
	Where(query interface{}, args ...interface{}) (tx *gorm.DB)
	Not(query interface{}, args ...interface{}) (tx *gorm.DB)
	Table(name string, args ...interface{}) (tx *gorm.DB)
	Order(value interface{}) (tx *gorm.DB)
	Limit(limit int) (tx *gorm.DB)
	Offset(offset int) (tx *gorm.DB)
	Distinct(args ...interface{}) (tx *gorm.DB)
	Joins(query string, args ...interface{}) (tx *gorm.DB)
	Group(name string) (tx *gorm.DB)
	Having(query interface{}, args ...interface{}) (tx *gorm.DB)
	Select(query interface{}, args ...interface{}) (tx *gorm.DB)
	Save(value interface{}) (tx *gorm.DB)
	Preload(query string, args ...interface{}) (tx *gorm.DB)
	Association(column string) *gorm.Association
}

type GormConnectionInt interface {
	CloseConnection()
}

type MysqlConnection struct {
	Url      string
	Database string
}

type GormConnection struct {
	Connection *sql.DB
}

var Connection = make(map[string]*MysqlConnection)

func AddConnection(ConnectionName string, MysqlUrl string) {
	Connection[ConnectionName] = &MysqlConnection{
		Url: MysqlUrl,
	}
}

func (a *MysqlConnection) ConnectDB() (MysqlConInt, error) {
	//user:password@tcp(localhost:5555)/dbname
	db, err := sql.Open("mysql", a.Url)

	if err != nil {
		return nil, err
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db, nil
}

func (a *MysqlConnection) OrmConnection() (GormInt, GormConnectionInt, error) {
	db, err := sql.Open("mysql", a.Url)

	if err != nil {
		return nil, nil, errors.New("Error OpenConnectin: " + err.Error())
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn: db,
	}), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})

	con, err := gormDB.DB()
	if err != nil {
		return nil, nil, errors.New("Error GetConnection: " + err.Error())
	}
	gormConnection := GormConnection{
		Connection: con,
	}
	return gormDB, gormConnection, nil
}

func (a GormConnection) CloseConnection() {
	if a.Connection != nil {
		_ = a.Connection.Close()
	}
}
