package postgre

import (
	"context"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

type Options = pg.Options
type Db = pg.DB
type Query = pg.Query

type PostgreConnection struct {
	Url string
	Con *Db
	Query *pg.Query
}

type PgQuery struct{
	pg.Query
}

var PostgreCon *PostgreConnection

type DbInt interface{
	Model(model ...interface{}) *pg.Query
}

type QueryInt interface{
	Returning(s string, params ...interface{}) *pg.Query
	Insert(values ...interface{}) (orm.Result, error)
}

func SetConnection(UrlConnection string) {
	PostgreCon = &PostgreConnection{
		Url: UrlConnection,
	}
	ctx := context.Background()

	opt, err := pg.ParseURL(PostgreCon.Url)
	if err != nil {
		panic(err.Error())
	}
	db:= pg.Connect(opt)
	defer db.Close()
	if err := db.Ping(ctx); err != nil {
		panic(err.Error())
	}
}

func (x *PostgreConnection) CreateConnection() DbInt {
	opt, err := pg.ParseURL(x.Url)
	if err != nil {
		return nil
	}
	x.Con = pg.Connect(opt)
	return x.Con
}

func (x *PostgreConnection) SetModel(model *pg.Query) *Query {
	return model
}

func (x *PostgreConnection) Model(model *pg.Query) *PostgreConnection {
	x.Query = model
	return x
}

func (x *PostgreConnection) Set(set string, params ...interface{}) *PostgreConnection {
	x.Query =  x.Query.Set(set, params)
	return x
}

func (x *PostgreConnection) OnConflict(s string, params ...interface{}) *PostgreConnection {
	x.Query =  x.Query.OnConflict(s, params)
	return x
}

func (x *PostgreConnection) Where(condition string, params ...interface{}) *PostgreConnection {
	x.Query =  x.Query.Where(condition, params)
	return x
}

func (x *PostgreConnection) Select(values ...interface{}) error {
	return x.Query.Select(values)
}

func (x *PostgreConnection) Insert(values ...interface{}) (orm.Result, error) {
	return x.Query.Insert(values)
}

func (x *PostgreConnection) Close() error {
	return x.Con.Close()
}



