package redis

import (
	"github.com/go-redis/redis"
	"time"
)

type RedisConnection struct {
	Url			string
}

var Connection = make(map[string]*RedisConnection)

func AddConnection(ConnectionName string, data RedisConnection){
	Connection[ConnectionName] = &data
}


func (a *RedisConnection) ConnectRedis() (*redis.Client){
	//"redis://<user>:<pass>@localhost:6379/<db>"
	opt, err := redis.ParseURL(a.Url)
	if err != nil {
		panic(err)
	}
	return redis.NewClient(opt)
}

func (a *RedisConnection) Get(Key string) (*string, error){
	con:= a.ConnectRedis()
	defer con.Close()
	val, err:= con.Get(Key).Result()
	if err != nil{
		return nil, err
	}
	return &val, nil
}

func (a *RedisConnection) Set(Key string, Val string, Duration time.Duration) error{
	con:= a.ConnectRedis()
	defer con.Close()
	err:= con.Set(Key, Val, Duration).Err()
	if err != nil{
		return err
	}
	return nil
}

func (r *RedisConnection) IncrBy(key string, incr int) (int, error) {
	con:= r.ConnectRedis()
	defer con.Close()
	res := con.Do("INCRBY", key, incr)
	return res.Int()
}