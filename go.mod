module gitlab.com/afey13/gocommon

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-pg/pg/v10 v10.10.6
	github.com/go-playground/validator/v10 v10.8.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.17.0 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/sirupsen/logrus v1.8.1
	github.com/unrolled/render v1.4.0
	go.mongodb.org/mongo-driver v1.5.4
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.12
)
