package middlewares

import (
	"context"
	"encoding/json"
	jwt2 "github.com/dgrijalva/jwt-go"
	"gitlab.com/afey13/gocommon/jwt"
	"gitlab.com/afey13/gocommon/logging"
	"gitlab.com/afey13/gocommon/response"
	"net/http"
)

func MiddlewareAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx:= r.Context()
		jwtAuth := jwt.JwtAuth{}
		Log := logging.GetDataLogging()
		token, err := jwt.VerifyToken(r)
		if err != nil{
			response.Response(w, response.ParameterResponse{
				Log:    &Log,
				Status: http.StatusUnauthorized,
				Message: "please login first",
			})
			return
		}
		claims, _ := token.Claims.(jwt2.MapClaims)
		result, _ := json.Marshal(claims)
		_ = json.Unmarshal(result,  &jwtAuth)
		ctx = context.WithValue(ctx, "auth", jwtAuth)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}