package middlewares

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/afey13/gocommon/logging"
	"gitlab.com/afey13/gocommon/prometheus"
)

type StatusRecorder struct {
	http.ResponseWriter
	Status  int
	Logging *logging.Data
}

func (r *StatusRecorder) WriteHeader(status int) {
	r.Status = status
	r.ResponseWriter.WriteHeader(status)
}

func Init(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/metrics" {
			dateTimeStart := time.Now()
			//init logging
			logging.NewLogging(r, dateTimeStart)
			//set for metric
			recorder := &StatusRecorder{
				ResponseWriter: w,
				Status:         200,
			}

			next.ServeHTTP(recorder, r)

			//after execute handler set value for metric
			routePattern := chi.RouteContext(r.Context()).RoutePattern()
			diff := time.Since(dateTimeStart)
			monitoring := prometheus.Getprometheus()
			monitoring.MetricRecord(strconv.Itoa(recorder.Status), r.Method, routePattern, diff)

		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func InitLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//init logging
		logging.NewLogging(r, time.Now())
		next.ServeHTTP(w, r)
	})
}

func GetValidateTokenFunc(headerName string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println(headerName)
			next.ServeHTTP(w, r)
		})
	}
}
