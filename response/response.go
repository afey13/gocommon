package response

import (
	"net/http"
	"time"

	"github.com/unrolled/render"
	"gitlab.com/afey13/gocommon/logging"
)

const UnexpectedError = "unexpected error please call administrator"

type JsonAPIResponse struct {
	//StatusCode int         `json:"status_code"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

type ParameterResponse struct {
	Log     *logging.Data
	Status  int
	Message string
	Data    interface{}
}

func Response(w http.ResponseWriter, data ParameterResponse) error {
	rdr := render.New()
	responseData := JsonAPIResponse{
		//StatusCode: data.Status,
		Message:    data.Message,
		Data:       data.Data,
	}
	if data.Status >= 500 {
		responseData.Message =  UnexpectedError
	}
	diff := time.Since(data.Log.TimeStart).Seconds()
	data.Log.StatusCode = data.Status
	data.Log.ExecTime = diff
	data.Log.Response = responseData
	data.Log.PrintLog()
	return rdr.JSON(w, data.Status, responseData)
}
