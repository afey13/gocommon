package prometheus

import (
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Prometheus struct {
	reqs    *prometheus.CounterVec
	latency *prometheus.HistogramVec
}

var ptheus *Prometheus

func Newprometheus() {
	DefaultBuckets := []float64{0.3, 1.2, 5.0}
	str := []string{"code", "method", "path"}
	p := Prometheus{}
	p.reqs = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name:        "http_requests_total",
			Help:        "How many HTTP requests processed, partitioned by status code, method and HTTP path.",
			ConstLabels: prometheus.Labels{"service": os.Getenv("SERVICE_NAME")},
		},
		str,
	)

	p.latency = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:        "http_request_duration_seconds",
		Help:        "How long it took to process the request, partitioned by status code, method and HTTP path.",
		ConstLabels: prometheus.Labels{"service": os.Getenv("SERVICE_NAME")},
		Buckets:     DefaultBuckets,
	},
		str,
	)

	ptheus = &p
}

//MetricRecord is recorder
func (p Prometheus) MetricRecord(code, method, path string, t time.Duration) {
	p.reqs.WithLabelValues(code, method, path).Inc()
	p.latency.WithLabelValues(code, method, path).Observe(float64(t.Nanoseconds()) / 1000000000)
}

func Getprometheus() Prometheus {
	return *ptheus
}
