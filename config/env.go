package config

import (
	"flag"
	"github.com/joho/godotenv"
	"log"
	"os"
	"path/filepath"
)

func SetupConfigEnv() {
	var BasePath, _ = os.Getwd()
	EnvLocal := flag.Bool("env-local", false, "display colorized output")
	flag.Parse()
	fileName :=  ".env"
	if *EnvLocal{
		fileName = ".env.local"
		log.Println("Using file ", fileName)
	}
	PathFileEnv := filepath.Join(BasePath, fileName)
	if _, err := os.Stat(PathFileEnv); !os.IsNotExist(err) {
		if err := godotenv.Load(PathFileEnv); err != nil {
			panic("Error loading " + fileName + " file")
		}
	}else{
		panic("File " + PathFileEnv + " not found")
	}
}
