package utils

import "encoding/json"

func StructToMap(data interface{}) map[string]interface{} {
	var inInterface map[string]interface{}
	inrec, _ := json.Marshal(data)
	json.Unmarshal(inrec, &inInterface)
	return inInterface
}
