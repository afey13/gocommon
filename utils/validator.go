package utils

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"gitlab.com/afey13/gocommon/logging"
	"strconv"
	"strings"
)

const max_length_passwd = 8
const max_length_phone = 8

type CustomValidator struct {
	Log		  *logging.Data
	Err       error
	validator *validator.Validate
}

type CustomValidatorInt interface {
	Validate(i interface{}) bool
	PrintError() string
}

func NewCustomValidator(Log *logging.Data) CustomValidatorInt{
	v := validator.New()

	_ = v.RegisterValidation("phone", func(fl validator.FieldLevel) bool {
		phone:= fl.Field().String()
		_, err := strconv.Atoi(phone)
		return len(phone) >= max_length_phone && err == nil
	})

	_ = v.RegisterValidation("passwd", func(fl validator.FieldLevel) bool {
		return len(fl.Field().String()) >= max_length_passwd
	})

	return &CustomValidator{
		Log: Log,
		Err: nil,
		validator: v,
	}
}

func (cv *CustomValidator) Validate(i interface{}) bool {
	cv.Err = cv.validator.Struct(i)
	return cv.Err != nil
}

func (cv *CustomValidator) PrintError() string {
	for _, err := range cv.Err.(validator.ValidationErrors) {
		tag := strings.ToLower(err.Tag())
		field := strings.ToLower(err.Field())
		cv.Log.AddMessage(fmt.Sprintf("validation error = %v %v", tag, field))
		switch tag {
		case "required":
			return fmt.Sprintf("please insert %v", field)
		case "email":
			return fmt.Sprintf("format email in field %v is wrong", field)
		case "passwd":
			return fmt.Sprintf("password lenght min %v", max_length_passwd)
		case "phone":
			return fmt.Sprintf("insert phone number with minimal length %v and use number only", max_length_phone)
		default:
			return fmt.Sprintf("there are field no valid, call administrator immediately")
		}
	}
	return fmt.Sprintf("there are field no valid, please call administrator")
}
